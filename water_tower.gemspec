# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'water_tower/version'

Gem::Specification.new do |spec|
  spec.name          = "water_tower"
  spec.version       = WaterTower::VERSION
  spec.authors       = ["Thomas Vendetta"]
  spec.email         = ["thomas.vendetta@ajj.com"]
  spec.description   = %q{Processes media.}
  spec.summary       = %q{Processes media.}
  spec.homepage      = ""
  spec.license       = "MIT"

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"
  spec.add_dependency "mini_magick"
  spec.add_dependency "streamio-ffmpeg"
end
