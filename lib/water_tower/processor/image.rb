require 'mini_magick'

module WaterTower::Processor
  module Image

    IMAGE_VERSIONS = {
      :thumbnail_small => 50,
      :thumbnail_medium => 100,
      :thumbnail_large => 200,
      :thumbnail_xlarge => 600
    }

    # Takes a file or an object that returns a #path string...
    # Opens and creates many versions, returns back a key value store where:
    # version => { :image => image_file, :filename => "thumbnail_large.jpeg" }
    def self.generate_thumbnails(image, method='resize_to_fill')
      versioned_images = {}

      IMAGE_VERSIONS.each do |version, size|
        # Open the image 
        img = MiniMagick::Image.open(image.path)
        # Perform whatever image operations we have..
        img = self.send(method, img, size, size)
        # Convert it to a JPEG
        img.format "jpeg"

        versioned_images["#{version}.jpeg"] = { 
          :image => img, 
          :filename => "#{version}.jpeg" 
        }
      end
      p "Returning versioned images."
      versioned_images
    end

    # Taken from CarrierWave::Processing::MiniMagick 
    def self.resize_to_fill(img, width, height, gravity = "Center") 
      cols, rows = img[:dimensions]
      img.combine_options do |cmd|
        if width != cols || height != rows
          scale = [width/cols.to_f, height/rows.to_f].max
          cols = (scale * (cols + 0.5)).round
          rows = (scale * (rows + 0.5)).round
          cmd.resize "#{cols}x#{rows}"
        end
        cmd.gravity gravity
        cmd.extent "#{width}x#{height}" if cols != width || rows != height
      end
      img = yield(img) if block_given?
      img
    end
  end
end
