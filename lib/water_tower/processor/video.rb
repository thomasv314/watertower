require 'streamio-ffmpeg'

module WaterTower::Processor
  module Video
    #call-seq:
    #  OrderDeck::Media.transcode_thumbnail(video, tmp_file_location) => File
    #
    # Takes a FFMPEG::Movie object and writes a screenshot to the tmp_file_string path.
    # :args: ffmpeg_movie_object, tmp_file_location
    def self.transcode_thumbnail(video, tmp_file_string)
      File.open(video.screenshot("#{tmp_file_string}.jpg", seek_time: rand(video.duration)).path, "r")
    end

    #call-seq:
    #  OrderDeck::Media.transcode_sd_video(video, tmp_file_location) => File
    #
    # Takes a FFMPEG::Movie object and it transcodes to Standard Definition (640x480).. Returns a file object.
    # :args: media_id, ffmpeg_video_object, tmp_file_location
    def self.transcode_sd_video(mid, video, tmp_file_string)
      options = { resolution: "640x480" }
      enc_options = { preserve_aspect_ratio: :height }
      video_file = video.transcode("#{tmp_file_string}.mp4", options, enc_options) do |progress|
        OrderDeck::Processing.update(mid, "sd_video", progress)
      end
      File.open(video_file.path, "r")
    end

    #call-seq:
    #  OrderDeck::Media.transcode_hd_video(video, tmp_file_location) => File
    #
    # Takes a FFMPEG::Movie object and transcodes it to 720p (1280x720).. Returns a file object.. 
    # :args: media_id, ffmpeg_movie_object, tmp_file_location
    def self.transcode_hd_video(mid, video, tmp_file_string)
      options = { resolution: "1280x720" }
      enc_options = { preserve_aspect_ratio: :height } 
      video_file = video.transcode("#{tmp_file_string}.mp4", options, enc_options) do |progress|
        $redis.set "media_processing:#{mid}:hdv", progress
      end
      File.open(video_file.path, "r") 
    end
  end
end
